from django.urls import path
from .views import *

urlpatterns = [
    path('', main, name='main'),
    path('1', for_1, name='for_1'),
    path('2', for_2, name='for_2')]