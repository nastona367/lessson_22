from django.shortcuts import render
from django.http import HttpResponseRedirect

def for_1(request):
    return render(request, 'pages/m.html')

def for_2(request):
    return render(request, 'pages/m1.html')

def main(request):
    return HttpResponseRedirect('1')