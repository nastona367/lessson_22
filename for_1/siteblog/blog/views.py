from django.shortcuts import render
from django.http import HttpResponseRedirect

def some(request):
    return render(request, 'pages/main.html')

def main(request):
    return render(request, 'pages/main.html')
