from django.shortcuts import render
from django.http import HttpResponseRedirect

def main(request):
    return HttpResponseRedirect('index_view')